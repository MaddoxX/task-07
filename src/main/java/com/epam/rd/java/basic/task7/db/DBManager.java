package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String CONNECTION_URL = getURL();

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	static String getURL(){
		try {
			return Files.readString(Path.of("app.properties")).substring(15);
		} catch (IOException e){
			e.printStackTrace();
		}
		return null;
	}

	public List<User> findAllUsers() throws DBException {
		List<User> userList = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(CONNECTION_URL);
			 Statement state = con.createStatement();
			 ResultSet rs = state.executeQuery("SELECT * FROM users")) {
			while(rs.next()){
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				userList.add(user);
			}
		} catch (SQLException e) {
			// НУЖНО ЛОГИРОВАТЬ и выдать нормальное обьяснение в ДБексепшн
			e.printStackTrace();
			throw new DBException("No users in DB because ", e);
		}
		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		boolean isAdd = false;
		try (Connection con = DriverManager.getConnection(CONNECTION_URL);
			 PreparedStatement prepState = con.prepareStatement("INSERT INTO users VALUES (DEFAULT, ?)", Statement.RETURN_GENERATED_KEYS)) {
			prepState.setString(1, user.getLogin());
			int count = prepState.executeUpdate();
			if (count == 1) {
				try(ResultSet rs = prepState.getGeneratedKeys();){
					if (rs.next()){
						isAdd = true;
						user.setId(rs.getInt(1));
					}
				}
			}
		} catch (SQLException e) {
			// НУЖНО ЛОГИРОВАТЬ и выдать нормальное обьяснение в ДБексепшн
			e.printStackTrace();
			throw new DBException("User not added in DB because ", e);
		}
		return isAdd;
	}

	public boolean deleteUsers(User... users) throws DBException {
		boolean isDelete = false;
		Connection con = null;
		PreparedStatement prepState = null;
		try {
			con = DriverManager.getConnection(CONNECTION_URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			for (User user : users){
				prepState = con.prepareStatement("DELETE FROM users WHERE login = ?", Statement.RETURN_GENERATED_KEYS);
				prepState.setString(1, user.getLogin());
				int count = prepState.executeUpdate();
				isDelete = count == 1;
			}
			con.commit();
		}
		catch (SQLException e) {
			// НУЖНО ЛОГИРОВАТЬ и выдать нормальное обьяснение в ДБексепшн
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException("Team not added in DB because ", e);
		}
		finally {
			if (con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (prepState != null){
				try {
					prepState.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return isDelete;
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		try (Connection con = DriverManager.getConnection(CONNECTION_URL);
			 PreparedStatement prepState = con.prepareStatement("SELECT * FROM users WHERE login = ?")) {
			prepState.setString(1, login);
			try(ResultSet rs = prepState.executeQuery();){
				while(rs.next()){
					user.setId(rs.getInt("id"));
					user.setLogin(rs.getString("login"));
				}
			}
		} catch (SQLException e) {
			// НУЖНО ЛОГИРОВАТЬ и выдать нормальное обьяснение в ДБексепшн
			e.printStackTrace();
			throw new DBException("No user in DB because ", e);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		try (Connection con = DriverManager.getConnection(CONNECTION_URL);
			 PreparedStatement prepState = con.prepareStatement("SELECT * FROM teams WHERE name = ?")) {
			prepState.setString(1, name);
			try(ResultSet rs = prepState.executeQuery();){
				while(rs.next()){
					team.setId(rs.getInt("id"));
					team.setName(rs.getString("name"));
				}
			}
		} catch (SQLException e) {
			// НУЖНО ЛОГИРОВАТЬ и выдать нормальное обьяснение в ДБексепшн
			e.printStackTrace();
			throw new DBException("No team in DB because ", e);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teamsList = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(CONNECTION_URL);
			 Statement state = con.createStatement();
			 ResultSet rs = state.executeQuery("SELECT * FROM teams")) {
			while(rs.next()){
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				teamsList.add(team);
			}
		} catch (SQLException e) {
			// НУЖНО ЛОГИРОВАТЬ и выдать нормальное обьяснение в ДБексепшн
			e.printStackTrace();
			throw new DBException("No teams in DB because ", e);
		}
		return teamsList;
	}

	public boolean insertTeam(Team team) throws DBException {
		boolean isAdd = false;
		try (Connection con = DriverManager.getConnection(CONNECTION_URL);
			 PreparedStatement prepState = con.prepareStatement("INSERT INTO teams VALUES (DEFAULT, ?)", Statement.RETURN_GENERATED_KEYS)) {
			prepState.setString(1, team.getName());
			int count = prepState.executeUpdate();
			if (count == 1) {
				try(ResultSet rs = prepState.getGeneratedKeys();){
					if (rs.next()){
						isAdd = true;
						team.setId(rs.getInt(1));
					}
				}
			}
		} catch (SQLException e) {
			// НУЖНО ЛОГИРОВАТЬ и выдать нормальное обьяснение в ДБексепшн
			e.printStackTrace();
			throw new DBException("Team not added in DB because ", e);
		}
		return isAdd;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		boolean isAdd = false;
		Connection con = null;
		PreparedStatement prepState = null;
		try {
			con = DriverManager.getConnection(CONNECTION_URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			for (Team team : teams) {
				prepState = con.prepareStatement("INSERT INTO users_teams VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
				prepState.setInt(1, user.getId());
				prepState.setInt(2, team.getId());
				int count = prepState.executeUpdate();
				isAdd = count == 1;
			}
			con.commit();
		}
		catch (SQLException e) {
			// НУЖНО ЛОГИРОВАТЬ и выдать нормальное обьяснение в ДБексепшн
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException("Team not added in DB because ", e);
		}
		finally {
			if (con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (prepState != null){
				try {
					prepState.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return isAdd;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> userTeamList = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(CONNECTION_URL);
			 PreparedStatement prepState = con.prepareStatement("SELECT * FROM users_teams WHERE user_id = ?")) {
			prepState.setInt(1, user.getId());
			try(ResultSet rs = prepState.executeQuery();){
				while(rs.next()){
					Team team = new Team();
					team.setId(rs.getInt("user_id"));
					int idTeam = rs.getInt("team_id");
					try (PreparedStatement nextPrepState = con.prepareStatement("SELECT * FROM teams WHERE id = ?")){
						nextPrepState.setInt(1, idTeam);
						try (ResultSet nextRS = nextPrepState.executeQuery()){
							while (nextRS.next()){
								team.setName(nextRS.getString(2));
							}
						}
					}
					userTeamList.add(team);
				}
			}
		} catch (SQLException e) {
			// НУЖНО ЛОГИРОВАТЬ и выдать нормальное обьяснение в ДБексепшн
			e.printStackTrace();
			throw new DBException("No team in DB because ", e);
		}
		return userTeamList;
	}

	public boolean deleteTeam(Team team) throws DBException {
		boolean isDeleted = false;
		try (Connection con = DriverManager.getConnection(CONNECTION_URL);
			 PreparedStatement prepState = con.prepareStatement("DELETE FROM teams WHERE name = ?", Statement.RETURN_GENERATED_KEYS)) {
			prepState.setString(1, team.getName());
			int count = prepState.executeUpdate();
			if (count == 1) {
				isDeleted = true;
			}
		} catch (SQLException e) {
			// НУЖНО ЛОГИРОВАТЬ и выдать нормальное обьяснение в ДБексепшн
			e.printStackTrace();
			throw new DBException("Team not deleted from DB because ", e);
		}
		return isDeleted;
	}

	public boolean updateTeam(Team team) throws DBException {
		boolean isUpdated = false;
		try (Connection con = DriverManager.getConnection(CONNECTION_URL);
			 PreparedStatement prepState = con.prepareStatement("UPDATE teams SET name = ? WHERE id = ?", Statement.RETURN_GENERATED_KEYS)) {
			prepState.setString(1, team.getName());
			prepState.setInt(2, team.getId());
			int count = prepState.executeUpdate();
			if (count == 1) {
				isUpdated = true;
			}
		} catch (SQLException e) {
			// НУЖНО ЛОГИРОВАТЬ и выдать нормальное обьяснение в ДБексепшн
			e.printStackTrace();
			throw new DBException("Team not updated from DB because ", e);
		}
		return isUpdated;
	}
}